# Линейный поиск в массиве

def array_search(A: list, N: int, x: int):
    """"Осуществляет поиск числа х в массиве А от 0 до N-1 идекса включительно. Возврашает
    индекс х в массиве А. Или -1, если такого нет. Если в массиве несколько одинаковых элементаов х,
    то вернуть индекс первого по счету"""
    for k in range(N):  # Я буду переберат индексы в диапозоне от 0 до N не включительно
        if A[k] == x:   # Если у меня А к равный х
            return k    # Я сразу выхожу возвращая  к
    return -1


def tesr_array_search():
    A1 = [1, 2, 3, 4, 5]
    m = array_search(A1, 5, 8)
    if m == -1:
        print("#test1 - ok")
    else:
        print("#test1 - fail")
    A2 = [-1, -2, -3, -4, -5]
    m = array_search(A2, 5, -3)
    if m == 2:
        print("#test2 - ok")
    else:
        print("#test2 - fail")
    A3 = [10, 20, 30, 10, 10]
    m = array_search(A3, 5, 10)
    if m == 0:
        print("#test3 - ok")
    else:
        print("#test3 - fail")

tesr_array_search()