# Циклический сдвиг

A = [1, 2, 3, 4, 5]
tmp = A[0]
N = 5
for k in range(N - 1):  # Сдвиг влево
    A[k] = A[k + 1]
A[N - 1] = tmp
print(A)

B = [1, 2, 3, 4, 5]
tmp = B[N - 1]
for k in range(N - 2, -1, -1):  # Сдвиг право
    B[k + 1] = B[k]
B[0] = tmp
print(B)

# Решето Эротосфера
C = [True] * N
C[0] = C[1] = False
for k in range(2, N):
    if C[k]:
        for m in range(2 * k, N, k):
            C[m] = False

print(C)
# Лучше печать по индексам
for k in range(N):
    print(k, '-', "простое" if C[k] else "составное")  # Террарный оператор 
