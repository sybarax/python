def func():
    what = input()
    if what == '0':  # Сравниваем со строкой, а не с числом
        return what
    return func()  # Если условие не выполнено - вызываем ф-цию снова


print(func())
