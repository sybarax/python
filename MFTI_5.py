# Массивы (тип list)

A = [1, 2, 3, 4, 5]  # массив типа List
for x in A:  # бегать по массиву
    print(x, type(x))
    # x += 1
    # print(x)

print(A)

# Модели виды данных, есть изменяемые объекты а есть не изменяемые объекты

for k in range(5):  # range 3 параметра start stop step
    A[k] = A[k] * A[k]

A = [0] * 1000  # Создал массив на тысячу
top = 0
x = int(input())
while x != 0:  # Пока х не равен 0
    A[top] = x
    top +=1
    x = int(input())

# Распечатываю в обратном порядке
for k in range(-4,-1,-1):
    print(A[k])

