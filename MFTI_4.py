# Функции
def hello():
    print('Hello word')


hello()
f = hello  # ссылка на функцию
f()  # вызов функци hello


def hello2(name):  # функция с параметром
    print('Hello,', name)


hello2('Jon')


def hello3(name='World'):  # функция с параметром можно вызывать без передачи параметра
    print('Hello,', name)


hello3('ser')
hello3()
