# Функции с возвратом параметров
def max2(x, y):
    if x > y:
        return x
    # return y /можно так
    else:
        return y


def max3(x, y, z):
    return max2(x, max2(y, z))


print(max3(5, 2, 7))

# Duck typing

print(max3('asd', 'wed', 'asd'))


def hello_separated(name='world', separator='-'):
    print('hello,', name, sep=separator)


hello_separated('Jon', '***')

