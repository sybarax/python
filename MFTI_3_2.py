base = 2
x = int(input())
while x > 0:
    digit = x % base  # Взять последнию цифру
    print(digit, end='')
    x //= base    # Зачеркнуть последнию цифру
# Однопроходные алгоритмы
