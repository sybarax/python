# Метод грубой силы (Bruteforce)

x = int(input())


def is_simle_number(x):
    """ Определяет, является ли число простым. x - Целое положительное число.Если простое, то возвращает True, а иначе False """
    divisor = 2
    while divisor < x:
        if x % divisor == 0:
            return False
        divisor += 1
    return True


print(is_simle_number(x))


def factorize_number(x):
    divisor = 2
    while x > 1:
        if x % divisor == 0:
            print(divisor)
            x //= divisor
        